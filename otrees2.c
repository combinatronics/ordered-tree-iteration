/*
Generate all ordered trees with n nodes.

The ordered trees are stored using a link-based representation.
 - Each node stores a link to its first child
 - Each node stores a link to its next (i.e., right) sibling. 

In this implementation, a node does not store a pointer to its parent.
See otrees1.c for an implementation that does store parent pointers.

The code is loopless: it visits each successive tree in worst-case O(1)-time.
A default visit function is provided that output each tree as a Dyck word.

The trees are generated in a Gray code order using pulls.
A pull involves popping one node (i.e., removing its first child) then
pushing the subtree to another node (i.e., adding it as a first child).
Each successive tree is created from the previous via one or two pulls.

See the following paper by Paul Lapey and Aaron Williams from ISAAC 2022:
"Pop & push: Ordered tree iteration in O(1)-time"

Usage:
Compile the file with a C compiler, then run the result with parameter n.
For example, type the following in the command-line to generate all of the
ordered trees with 5 nodes:
 >> gcc otrees2.c
 >> ./a.out 5
*/

#include <stdio.h>
#include <stdlib.h>
typedef struct node { struct node *first; struct node *right; } node;

void pull(node *A, node *B){ 
  node *pulled = B->first;
  B->first = pulled->right;
  pulled->right = A->first;
  A->first = pulled;
}

void visitrec(node* n){ // prints an ordered tree as a Dyck word;
  node* child = n->first;
  while(child){
    putchar('1');
    visitrec(child);
    child = child->right;
    putchar('0');
  }
}
void visit(node* n){ visitrec(n); putchar('\n');}

void coolOtree(int n) {
  node *root = (node*)calloc(sizeof(node),1);
  node *prev, *curr = root;
  for(int i = 0; i < n; i++){
    curr->first = (node*)calloc(sizeof(node),1);
    prev=curr;
    curr=curr->first;
  }
  pull(root,prev);
  node *p=root, *g=NULL, *o = root->first->right;
  visit(root);
  while (o) {
    if (o->first) { // if o has a child, o pulls p
      pull(o,p);
      g=p;
      p=o;
      o = o->first->right;
    } else {
      if (p == root) { // if p is the root, o pulls p
        pull(o,p);
      } else { // otherwise, g pulls p and root pulls p
        pull(g,p);
        pull(root,p);
        p=root; //g could be set to null
      }
      o = o->right;
    }
    visit(root);
  }
}
int main(int argc, char **argv) { coolOtree(atoi(argv[1])); }
