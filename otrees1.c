/*
Generate all ordered trees with n nodes.

The ordered trees are stored using a link-based representation.
 - Each node stores a link to its first child
 - Each node stores a link to its next (i.e., right) sibling. 

In this implementation, a node also stores a pointer to its parent.
See otrees2.c for an implementation that does not store parent pointers.

The code is loopless: it visits each successive tree in worst-case O(1)-time.
A default visit function is provided that output each tree as a Dyck word.

The trees are generated in a Gray code order using pulls.
A pull involves popping one node (i.e., removing its first child) then
pushing the subtree to another node (i.e., adding it as a first child).
Each successive tree is created from the previous via one or two pulls.

See the following paper by Paul Lapey and Aaron Williams from ISAAC 2022:
"Pop & push: Ordered tree iteration in O(1)-time"

Usage:
Compile the file with a C compiler, then run the result with parameter n.
For example, type the following in the command-line to generate all of the
ordered trees with 5 nodes:
 >> gcc otrees1.c
 >> ./a.out 5
*/

#include <stdio.h>
#include <stdlib.h>
typedef struct node { struct node *parent, *first, *right; } node;

void pull(node *A, node *B){
  node *child = B->first;
  B->first = child->right;
  child->right = A->first;
  A->first = child;
  child->parent = A;
}

void visit(node* root){ // prints an ordered tree as a Dyck word
  node* child = root->first;
  while(child){
    putchar('1');
    visit(child);
    child = child->right;
    putchar('0');
  }
  if (!root->parent) putchar('\n');
}

void coolOtree(int n) {
  node* root = (node*)calloc(sizeof(node),1);
  node* curr = root;
  for(int i = 0; i < n; i++){
    curr->first = (node*)calloc(sizeof(node),1);
    curr->first->parent=curr;
    curr=curr->first;
  }
  pull(root,curr->parent);
  node *p, *o = root->first->right;
  visit(root);
  while (o) {
    p=o->parent;
    if (o->first) { // if o has a child, o pulls p
      pull(o,p);
      o = o->first->right;
    } else {
      if (p == root) { // if p is the root, o pulls p
        pull(o,p);
      } else { // otherwise, g pulls p and root pulls p
        pull(p->parent,p);
        pull(root,p);
      }
      o = o->right;
    }
    visit(root);
  }
}

int main(int argc, char **argv) { coolOtree(atoi(argv[1])); }
