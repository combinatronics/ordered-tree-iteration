# Ordered tree iteration

Generate all ordered trees with n nodes.

The ordered trees are stored using a link-based representation.
 - Each node stores a link to its first child
 - Each node stores a link to its next (i.e., right) sibling. 
 - [optional] Each node stores a link to its parent.

There are two implementations:
`otrees1.c` stores parent pointers; 
`otrees2.c` does not store parent pointers.

Both implementations are loopless: They visit each successive tree in worst-case O(1)-time.
A default visit function is provided that outputs each tree as a Dyck word[^1].

[^1]: Note that this default visit function takes O(n)-time.
If your goal is to generate Dyck words in O(1)-time (or binary trees in O(1)-time), then refer to citation [24] in the ISAAC 2022 paper.

The trees are generated in a Gray code order using pulls.
A *pull* involves popping one node (i.e., removing its first child) then
pushing the subtree to another node (i.e., adding it as a first child).
Each successive tree is created from the previous via one or two pulls.

See the following paper by Paul Lapey and Aaron Williams from ISAAC 2022:
"Pop & push: Ordered tree iteration in O(1)-time"

## Usage
Compile the file with a C compiler, then run the result with parameter n.
For example, type the following in the command-line to generate all of the
ordered trees with 4 nodes:
```
gcc otrees1.c
./a.out 4
```

This should result in the following output.
```
10111000
11011000
11101000
10110100
11010100
10101100
11001100
11100100
10110010
11010010
10101010
11001010
11100010
11110000
```
This output is created using the default visit function which outputs each ordered tree as a Dyck word.
Internally, the trees are generated using a link-based representation as described above.
